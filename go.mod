module gitlab.com/weather-station4/sensor-simulator

go 1.19

require github.com/caarlos0/env v3.5.0+incompatible

require github.com/caarlos0/env/v6 v6.10.1 // indirect
