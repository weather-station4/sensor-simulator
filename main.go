package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/caarlos0/env"
)

type config struct {
	Port     string `env:"PORT"`
	DeviceId string `env:"DEVICE_ID"`
	Mode     string `env:"MODE"`
}

type Sensor struct {
	Id string `json:"id"`
}

type SensorData struct {
	Value  float32 `json:"value"`
	Sensor Sensor  `json:"sensor"`
	Unit   string  `json:"unit"`
}

type Message struct {
	Device     string       `json:"device_id"`
	TimeStamp  time.Time    `json:"time_stamp"`
	SensorData []SensorData `json:"sensor_data"`
}

var cfg config

func getSensor() Sensor {
	return Sensor{
		Id: "MHT22",
	}
}

func floatrandom(value_1, value_2 float32) float32 {
	return value_1 + value_2 + rand.Float32()
}

func generateData() float32 {
	hourI := time.Now().Hour()
	hour := float32(hourI)
	return -(hour * hour * 0.1) + 20 + (2.5 * hour)
}

func generateSensorData(unit string) SensorData {
	var value float32
	if cfg.Mode == "DEFAULT" {
		value = generateData()
	} else {
		value = floatrandom(50, 2)
	}
	return SensorData{
		Sensor: getSensor(),
		Unit:   unit,
		Value:  value,
	}
}

func generateMessage() Message {
	var sensorData []SensorData
	sensorData = append(sensorData, generateSensorData("Celsius"))
	sensorData = append(sensorData, generateSensorData("Percent"))
	return Message{
		Device:     cfg.DeviceId,
		TimeStamp:  time.Now(),
		SensorData: sensorData,
	}
}

func getSensorData(w http.ResponseWriter, r *http.Request) {
	sensorBytes, err := json.Marshal(generateMessage())
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(sensorBytes)
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func getFromFile(key string) (string, error) {
	file, err := os.Open(".env")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), "=")
		if s[0] == key {
			return s[1], nil
		}
	}
	return "", fmt.Errorf("key %s not found", key)
}

func setConfig() {
	cfg = config{}
	var err error = nil
	if err := env.Parse(&cfg); err != nil {
		fmt.Printf("%+v\n", err)
	}
	if cfg.Port == "" {
		cfg.Port, err = getFromFile("PORT")
		if err != nil {
			panic("PORT is not set")
		}
	}
	if cfg.DeviceId == "" {
		cfg.DeviceId, err = getFromFile("DEVICE_ID")
		if err != nil {
			panic("DEVICE_ID is not set")
		}
	}
	if cfg.Mode == "" {
		mode, err := getFromFile("MODE")
		if err != nil || mode != "" {
			cfg.Mode = mode
		}
	}
}

func main() {
	setConfig()
	fmt.Println(cfg)

	sensor := http.NewServeMux()
	sensor.HandleFunc("/", getSensorData)
	health := http.NewServeMux()
	health.HandleFunc("/", healthCheck)
	mux := http.NewServeMux()
	mux.Handle("/health", health)
	mux.Handle("/sensor", sensor)

	err := http.ListenAndServe(fmt.Sprintf(":%s", cfg.Port), mux)
	if err != nil {
		panic(err)
	}
}
